<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    public static function carShow()
    {
    	return self::latest()->get();
    }
}
