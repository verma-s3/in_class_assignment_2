<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dog extends Model
{
    public static function tableShow(){
    	return self::latest()->get();
    }
}
