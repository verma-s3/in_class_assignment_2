@extends('cars.cars_layout')

@section('content')

<div class="col-lg-8">

	<h1 class="mt-4">Car Details</h1>

	<p class="lead">
		Name: {{$car->name}}
	</p>

	<p class="lead">
		Description: {{$car->description}}
	</p>
	
	<p class="lead">
		<img src="/images/{{$car->image}}">
	</p>
</div>

@endsection