@extends('sonia.sonia-layout')

@section('content')

<!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">Dog Detail</h1>

        <!-- Author -->
        <p class="lead">
          <strong>Name:</strong> {{$dog->name}}
        </p>
        <p class="lead">
          <strong>Ranking:</strong> {{$dog->id}}
        </p>

        <p  class="lead"><strong>Details:</strong> {{$dog->description}}</p>
        <p class="lead">
          <strong>Created at:</strong> {{Carbon\Carbon::parse($dog->created_at)->diffForhumans() }}
        </p>
        <p class="lead">
          <strong>Image</strong> 
        </p>
        <img src="/image/sonia/{{$dog->image}}" />
        

      </div>

@endsection