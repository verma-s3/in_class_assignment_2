<?php

use Illuminate\Database\Seeder;

class seed_dogs_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dogs')->insert([
        	'name' => 'Fido',
        	'description' =>'This is black and white puppy',
        	'image' => 'fido.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'Lassie',
        	'description' => 'This is a Lassie dog',
        	'image' => 'lassie.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'Pug',
        	'description' => 'This is a small dog',
        	'image' => 'pug.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'German Shepperd',
        	'description' => 'This is german shepperd',
        	'image' => 'german.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'Russel',
        	'description' => 'This is a russel dog',
        	'image' => 'russel.jpg'
        ]);
    }
}
