<?php

use Illuminate\Database\Seeder;

class seed_planes_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('planes')->insert([
          'name' => 'Boeing',
          'description' => 'Boeing is the best plane ever',
          'image' => 'boeing.jpg'
        ]);
        \DB::table('planes')->insert([
          'name' => 'Raffle',
          'description' => 'Raffale is the excellent plane ever',
          'image' => 'raffle.jpg'
        ]);
        \DB::table('planes')->insert([
          'name' => 'TurboProp',
          'description' => 'Boeing is the WOrst plane ever',
          'image' => 'turbo.jpg'
        ]);
        \DB::table('planes')->insert([
          'name' => 'Wide-Body',
          'description' => 'Wide-Body is the good plane ',
          'image' => 'wb.jpg'
        ]);
        \DB::table('planes')->insert([
          'name' => 'MIG-21',
          'description' => 'MIG-21 is the bestest plane ever',
          'image' => 'mig.jpg'
        ]);                                
    }
}
