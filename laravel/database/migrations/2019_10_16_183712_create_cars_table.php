<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
         Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('package_type');
            $table->string('session_time');
            $table->string('photo_quality');
            $table->string('photo_size');
            $table->integer('no_of_photos');
            $table->string('delivery_method');
            $table->double('price',8,2);
            $table->string('image');
            $table->string('photographer_name');
            $table->text('description');
            $table->enum('availability',['Yes','No']);
            $table->timestamps('created_at')->useCurrent();
         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
